## 项目说明

GitCode代码托管平台是一款面向专业开发者的高效云端开发工具。平台集成了先进的代码托管服务，支持全面的版本控制、分支管理和协作开发功能。开发者可以通过GitCode便捷地上传、管理和分享代码仓库，享受无缝的协同开发体验。平台还提供了丰富的开源组件库和多样化的开发工具，无论是个人项目还是企业级应用，GitCode都能为用户提供安全、稳定和高效的代码托管解决方案，助力开发者实现卓越的代码管理与协作。

即使这样，但是针对OpenHarmony、HarmonyOS、Android、以及IOS都没有对应的应用去给用户带来更好的体验，这不，GitCode-X就来了。

## 项目架构

![image-20240828194431938](https://luckly007.oss-cn-beijing.aliyuncs.com/uPic/image-20240828194431938.png)



## 效果

![image-20240829075134757](https://luckly007.oss-cn-beijing.aliyuncs.com/uPic/image-20240829075134757.png)

![image-20240829075223694](https://luckly007.oss-cn-beijing.aliyuncs.com/uPic/image-20240829075223694.png)



![image-20240829075248116](https://luckly007.oss-cn-beijing.aliyuncs.com/uPic/image-20240829075248116.png)



## 开发计划


OAuth登陆（已实现）

获取用户的资料（）

搜索某个用户

列出授权用户所属的组织

查看仓库数据

## OpenHarmony

OpenHarmony是由开放原子开源基金会（OpenAtom Foundation）孵化及运营的开源项目，目标是面向全场景、全连接、全智能时代，搭建一个智能终端设备操作系统的框架和平台，促进万物互联产业的繁荣发展。

详细说明：https://gitcode.com/org/openharmony/

## HarmonyOS

## 更新日志

**2024/07/25**

1. 工具首次发布

## 联系我们

坚果派由坚果等人创建，团队拥有12个华为HDE带领热爱HarmonyOS/OpenHarmony的开发者，以及若干其他领域的三十余位万粉博主运营。专注于分享HarmonyOS/OpenHarmony、ArkUI-X、元服务、仓颉。团队成员聚集在北京，上海，南京，深圳，广州，宁夏等地，目前已开发鸿蒙原生应用/三方库60+，欢迎交流。

电话：17752170152



## 问题反馈与建议

强烈推荐阅读 [《提问的智慧》](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way)、[《如何向开源社区提问题》](https://github.com/seajs/seajs/issues/545) 和 [《如何有效地报告 Bug》](http://www.chiark.greenend.org.uk/~sgtatham/bugs-cn.html)、[《如何向开源项目提交无法解答的问题》](https://zhuanlan.zhihu.com/p/25795393)，更好的问题更容易获得帮助。





## 贡献者们

李学进

坚果



## 开发交流

电话：17752170152



## gitcode使用文档

https://docs.gitcode.com/

https://docs.gitcode.com/docs/openapi/orgs/

遇到平台bug，大家[可以在提Issue](https://gitcode.com/Gitcode-offical-team/GitCode-Docs/issues)
