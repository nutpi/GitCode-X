## 创建oauth应用

今天我们一起来创建oauth，[首先登陆](https://gitcode.com/setting/oauth)，然后我们下一步

![image-20240828084910827](https://luckly007.oss-cn-beijing.aliyuncs.com/uPic/image-20240828084910827.png)

点击创建应用后，完成如下信息的填写。

![image-20240828085207252](https://luckly007.oss-cn-beijing.aliyuncs.com/uPic/image-20240828085207252.png)





![image-20240828085324212](https://luckly007.oss-cn-beijing.aliyuncs.com/uPic/image-20240828085324212.png)

https://blog.csdn.net/qq_39132095

创建以后，会生成Client ID和Client Secret

Client ID

73863f25ecaa42bdac201d376c6264e9

Client Secret

3ca4fc2140014916b4430f854bcf8ecf



接下来我们就开始使用。



## gitcode使用文档

https://docs.gitcode.com/

API文档

https://docs.gitcode.com/docs/openapi/



接下来我们先用OAuth实现登陆功能



## 登陆功能

https://gitcode.com/oauth/authorize?client_id=73863f25ecaa42bdac201d376c6264e9&redirect_uri=https://blog.csdn.net/qq_39132095&response_type=code&state=state_ok





{"access_token":"x3HRtSY8U39gfq42LyhDtnPT","expires_in":1296000,"refresh_token":"7686c3cde5874a6aadd6f56deffc06fd","scope":"all_user all_key all_groups all_pr all_issue all_note all_hook all_repository all_projects","created_at":"2024-08-28T07:37:45.377Z"}





## 个人信息

```js
{
  "avatar_url": "https://cdn-img.gitcode.com/ba/da/5b7a7d5ab1e41b0284e4ec850f38960292ed8c1b993961a3a6624a64995b8d3e.jpg",
  "followers_url": "https://api.gitcode.com/api/v5users/qq_39132095/followers",
  "html_url": "https://gitcode.com/qq_39132095",
  "id": "66150a30431454566491f8ef",
  "login": "qq_39132095",
  "name": "坚果的博客",
  "type": "User",
  "url": "https://api.gitcode.com/api/v5/qq_39132095",
  "bio": "华为开发者专家（HDE），润开鸿生态技术专家， OpenHarmony布道师，OpenHarmony校源行开源大使，InfoQ签约作者，电子发烧友鸿蒙MVP，51CTO博客专家博主，阿里云博客专家，专注于前端技术的分享，包括鸿蒙，ArkUI-X，Flutter,小程序，如果你迷茫，不妨来瞅瞅码农的轨迹，也期待你加入坚果派！",
  "blog": "",
  "company": "",
  "email": "852851198@qq.com",
  "followers": 0,
  "following": 1
}
```

## 状态码

GitCode API 旨在根据上下文和操作返回不同的状态码。这样，如果请求导致错误，你可以了解出了什么问题。

下表概述了 GitCode API 功能的一般行为：

| 请求类型      | 描述                                                         |
| ------------- | ------------------------------------------------------------ |
| `GET`         | 访问一个或多个资源并以 JSON 形式返回结果                     |
| `POST`        | 如果资源成功创建，则返回 `201 Created` 并以 JSON 形式返回新创建的资源 |
| `GET` / `PUT` | 如果资源被成功访问或修改，则返回 `200 OK`。返回的（修改后的）结果以 JSON 形式返回 |
| `DELETE`      | 如果资源成功删除，则返回 `204 No Content`；如果资源计划被删除，则返回 `202 Accepted` |

下表显示了 GitCode API 请求可能的返回代码：

| 返回值                    | 描述                                                         |
| ------------------------- | ------------------------------------------------------------ |
| `200 OK`                  | `GET`、`PUT` 或 `DELETE` 请求成功，并且资源本身以 JSON 形式返回 |
| `201 Created`             | `POST` 请求成功，并且资源以 JSON 形式返回                    |
| `202 Accepted`            | `GET`、`PUT` 或 `DELETE` 请求成功，并且资源计划进行处理      |
| `204 No Content`          | 服务器已成功满足请求，并且在响应负载体中没有额外的内容发送   |
| `301 Moved Permanently`   | 资源已被定位到由 `Location` 头给出的 URL                     |
| `304 Not Modified`        | 资源自上次请求以来未被修改                                   |
| `400 Bad Request`         | API 请求的必需属性缺失。例如，未给出问题的标题               |
| `401 Unauthorized`        | 用户未经认证。需要有效的用户令牌                             |
| `403 Forbidden`           | 请求不被允许。例如，用户不允许删除项目                       |
| `404 Not Found`           | 无法访问资源。例如，无法找到资源的 ID，或者用户无权访问资源  |
| `405 Method Not Allowed`  | 不支持请求                                                   |
| `409 Conflict`            | 冲突的资源已存在。例如，创建已存在名称的项目                 |
| `412 Precondition Failed` | 请求被拒绝。这可能发生在尝试删除在此期间被修改的资源时，如果提供了 `If-Unmodified-Since` 头 |
| `422 Unprocessable`       | 无法处理实体                                                 |
| `429 Too Many Requests`   | 用户超过了应用程序[速率限制](https://docs.gitcode.com/docs/api/guide/#速率限制) |
| `500 Server Error`        | 处理请求时，服务器上出了问题                                 |
| `503 Service Unavailable` | 服务器无法处理请求，因为服务器暂时过载                       |



## 组织

```
[
  {
    "avatar_url": "https://cdn-img.gitcode.com/ac/dd/18910ac988ddca046a031a77328253362982e28b75d0b149bea23eb5647cf515.png?time1724847204717",
    "description": "",
    "id": 2333322,
    "login": "nutpi",
    "path": "nutpi",
    "name": "坚果派",
    "url": "https://gitcode.com/nutpi"
  },
  {
    "avatar_url": "https://cdn-img.gitcode.com/cf/bf/349c8fbf998f96f60e10d8918239dfe678f9e78cdc4d07701efdd591ebbed7cb.jpg?time1715738758513",
    "description": "仓颉编程语言是一款面向全场景智能的新一代编程语言，主打原生智能化、天生全场景、高性能、强安全。",
    "id": 1591596,
    "login": "Cangjie",
    "path": "Cangjie",
    "name": "Cangjie",
    "url": "https://gitcode.com/Cangjie"
  }
]
```

## 获取授权token



```
{
  "access_token": "Q7Cfw9yEhwWjyrPu-zmifaV6",
  "expires_in": 1296000,
  "refresh_token": "523ab54897524dc691c704679240f1cf",
  "scope": "all_user all_key all_groups all_pr all_issue all_note all_hook all_repository all_projects",
  "created_at": "2024-08-28T12:47:33.298Z"
}
```





https://api.gitcode.com/api/v5/orgs/nutpi







```json
{
  "path": "nutpi",
  "avatar_url": "https://cdn-img.gitcode.com/ac/dd/18910ac988ddca046a031a77328253362982e28b75d0b149bea23eb5647cf515.png?time1724847204717",
  "created_at": "2024-08-28T20:13:30.933+08:00",
  "description": "坚果派由坚果等人创建，团队拥有12个华为HDE以及2700+HarmonyOS开发者，以及若干其他领域的三十余位万粉博主/UP主运营。\n专注于分享的技术包括HarmonyOS/OpenHarmony、仓颉、ArkUI-X、元服务、AI、BlueOS操作系统、团队成员聚集在北京，上海，南京，深圳，广州，苏州、长沙、宁夏等地，目前已为华为、vivo、腾讯、亚马逊以及三方技术社区提供各类开发咨询服务100+。累计粉丝100+w，孵化开发者10w+，高校20+、企业10+。自研应用14款，三方库38个，鸿蒙原生应用课程500+。持续助力鸿蒙仓颉等生态繁荣发展。\n",
  "enterprise": "nutpi",
  "follow_count": 1,
  "id": 2333322,
  "login": "nutpi",
  "members": 2,
  "name": "坚果派",
  "owner": {
    "id": 0
  },
  "private_repos": 0,
  "public": true,
  "public_repos": 0,
  "type": "Group"
}
```



## 参考

- [微信小程序官方文档](https://developers.weixin.qq.com/miniprogram/dev/framework/)
- [百度智能小程序官方文档](https://smartprogram.baidu.com/docs/develop/tutorial/intro/)
- [支付宝小程序官方文档](https://docs.alipay.com/mini/developer/getting-started)
- [字节跳动小程序官方文档](https://microapp.bytedance.com/)
- [京东小程序官方文档](https://mp.jd.com/docs/dev/)
- [QQ 小程序官方文档](https://q.qq.com/wiki/)
